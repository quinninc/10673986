function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
  /*!*****************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content>\r\n  <div class=\"login\">\r\n    <ion-card class=\"card\">\r\n      <ion-card-header>\r\n        <ion-card-title>Login</ion-card-title>\r\n      </ion-card-header>\r\n      <ion-card-content>\r\n        <form class=\"form\" [formGroup]=\"validations_form\" (ngSubmit)=\"loginUser(validations_form.value)\">\r\n          <ion-item>\r\n\r\n            <ion-label position=\"floating\" color=\"primary\">NSBM Email Address</ion-label>\r\n            <ion-input class=\"textinput\" type=\"text\" formControlName=\"email\" ngModel\r\n              pattern=\"(\\W|^)[\\w.+\\-]*@(students.)?nsbm\\.lk(\\W|$)\">\r\n            </ion-input>\r\n          </ion-item>\r\n\r\n          <div class=\"validation-errors\">\r\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\r\n              <div class=\"error-message\"\r\n                *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\r\n                {{ validation.message }}\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n          <ion-item>\r\n\r\n            <ion-label position=\"floating\" color=\"primary\">Password</ion-label>\r\n            <ion-input class=\"textinput\" type=\"password\" formControlName=\"password\" class=\"form-controll\" required>\r\n            </ion-input>\r\n          </ion-item>\r\n\r\n          <div class=\"validation-errors\">\r\n            <ng-container *ngFor=\"let validation of validation_messages.password\">\r\n              <div class=\"error-message\"\r\n                *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\r\n                {{ validation.message }}\r\n              </div>\r\n            </ng-container>\r\n            <ion-button shape=\"round\" fill=\"outline\" color=\"success\" expand=\"full\" class=\"submit-btn\" type=\"submit\"\r\n              [disabled]=\"!validations_form.valid\" style=\"text-align: center;\">Log In</ion-button>\r\n\r\n            <label class=\"error-message\">{{errorMessage}}</label>\r\n          </div>\r\n        </form>\r\n        <p class=\"go-to-register\">\r\n          No account yet? <a (click)=\"goToRegisterPage()\">Create an account.</a>\r\n        </p>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </div>\r\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/login/login.module.ts":
  /*!***************************************!*\
    !*** ./src/app/login/login.module.ts ***!
    \***************************************/

  /*! exports provided: LoginPageModule */

  /***/
  function srcAppLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
      return LoginPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./login.page */
    "./src/app/login/login.page.ts");

    var routes = [{
      path: '',
      component: _login_page__WEBPACK_IMPORTED_MODULE_6__["Login"]
    }];

    var LoginPageModule = function LoginPageModule() {
      _classCallCheck(this, LoginPageModule);
    };

    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["Login"]]
    })], LoginPageModule);
    /***/
  },

  /***/
  "./src/app/login/login.page.scss":
  /*!***************************************!*\
    !*** ./src/app/login/login.page.scss ***!
    \***************************************/

  /*! exports provided: default */

  /***/
  function srcAppLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".login {\n  padding: 1px;\n  height: 100%;\n  width: 100%;\n  background-image: url(/assets/SchoolBag.png);\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.card {\n  width: 90%;\n  text-align: center;\n  margin-top: 50%;\n  margin-left: auto;\n  margin-right: auto;\n  font-size: x-large;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vQzpcXFVzZXJzXFxyYW51bFxcRG9jdW1lbnRzXFxHaXRIdWJcXDJuZCBZZWFyXFwybmQgU2VtZXN0ZXJcXFVPUF9TRV9ZMlMxLVBVU0wyMDAzX0lOVEVHUkFUSU5HX1BST0pFQ1RcXEFwcGxpY2F0aW9uXFxNb2JpbGUgQXBwcyAoUmFudWwgLSAxMDY3Mzk4NilcXFdvcmtzcGFjZVxcTlNCTS1IVUIvc3JjXFxhcHBcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDRDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBREVBO0VBQ0ksVUFBQTtFQUNDLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0NMIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luIHtcclxuICAgIHBhZGRpbmc6IDFweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvU2Nob29sQmFnLnBuZyk7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICBcclxufVxyXG4uY2FyZCB7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICBtYXJnaW4tdG9wOiA1MCUgO1xyXG4gICAgIG1hcmdpbi1sZWZ0OiBhdXRvIDtcclxuICAgICBtYXJnaW4tcmlnaHQ6IGF1dG8gO1xyXG4gICAgIGZvbnQtc2l6ZTogeC1sYXJnZTtcclxufVxyXG4iLCIubG9naW4ge1xuICBwYWRkaW5nOiAxcHg7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgvYXNzZXRzL1NjaG9vbEJhZy5wbmcpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uY2FyZCB7XG4gIHdpZHRoOiA5MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogNTAlO1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICBmb250LXNpemU6IHgtbGFyZ2U7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/login/login.page.ts":
  /*!*************************************!*\
    !*** ./src/app/login/login.page.ts ***!
    \*************************************/

  /*! exports provided: Login */

  /***/
  function srcAppLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Login", function () {
      return Login;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _services_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./../services.service */
    "./src/app/services.service.ts");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.cjs.js");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/fire/firestore */
    "./node_modules/@angular/fire/firestore/es2015/index.js");

    var Login = /*#__PURE__*/function () {
      function Login(navCtrl, authService, formBuilder, loadingController, firestore, alertController) {
        _classCallCheck(this, Login);

        this.navCtrl = navCtrl;
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.firestore = firestore;
        this.alertController = alertController;
        this.errorMessage = "";
        this.show = false;
        this.slideOpts = {
          initialSlide: 1,
          speed: 400
        };
        this.validation_messages = {
          email: [{
            type: "required",
            message: "Your NSBM University Email is required."
          }, {
            type: "pattern",
            message: "Please enter a valid email."
          }],
          password: [{
            type: "required",
            message: "Password is required."
          }, {
            type: "minlength",
            message: "Password must be at least 5 characters long."
          }]
        };
      }

      _createClass(Login, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          firebase__WEBPACK_IMPORTED_MODULE_3__["auth"]().onAuthStateChanged(function (user) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (user) {
                        this.firestore.collection('/users/userTypes/studentUsers').doc(this.authService.userDetails().email).ref.get().then(function (doc) {
                          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            var loading, _yield$loading$onDidD, role, data, _loading, _yield$_loading$onDid, _role, _data, alert;

                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    if (!(doc.data().status.toString() == "Active")) {
                                      _context.next = 17;
                                      break;
                                    }

                                    // User is signed in.
                                    console.log('User is signed in');
                                    _context.next = 4;
                                    return this.loadingController.create({
                                      message: 'Please wait...',
                                      duration: 2000
                                    });

                                  case 4:
                                    loading = _context.sent;
                                    _context.next = 7;
                                    return loading.present();

                                  case 7:
                                    _context.next = 9;
                                    return loading.onDidDismiss();

                                  case 9:
                                    _yield$loading$onDidD = _context.sent;
                                    role = _yield$loading$onDidD.role;
                                    data = _yield$loading$onDidD.data;
                                    console.log('Loading dismissed!');
                                    this.userEmail = this.authService.userDetails().email;
                                    this.navCtrl.navigateForward("tabs/home");
                                    _context.next = 34;
                                    break;

                                  case 17:
                                    this.authService.logoutUser();
                                    _context.next = 20;
                                    return this.loadingController.create({
                                      message: 'Session Closing...',
                                      duration: 2000
                                    });

                                  case 20:
                                    _loading = _context.sent;
                                    _context.next = 23;
                                    return _loading.present();

                                  case 23:
                                    _context.next = 25;
                                    return _loading.onDidDismiss();

                                  case 25:
                                    _yield$_loading$onDid = _context.sent;
                                    _role = _yield$_loading$onDid.role;
                                    _data = _yield$_loading$onDid.data;
                                    console.log('Loading dismissed!');
                                    _context.next = 31;
                                    return this.alertController.create({
                                      header: 'Account Disabled',
                                      subHeader: 'Contact Program Office',
                                      message: 'You cannot access the NSBM HUB as your account is disabled !',
                                      buttons: ['OK']
                                    });

                                  case 31:
                                    alert = _context.sent;
                                    _context.next = 34;
                                    return alert.present();

                                  case 34:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        });
                      } else {
                        // No user is signed in.
                        console.log('User is NOT signed in');
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
          this.validations_form = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(5), _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]))
          });
        }
      }, {
        key: "loginUser",
        value: function loginUser(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this3 = this;

            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    this.authService.loginUser(value).then(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                        var _this4 = this;

                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                          while (1) {
                            switch (_context4.prev = _context4.next) {
                              case 0:
                                console.log(res);
                                this.errorMessage = "";
                                this.firestore.collection('/users/userTypes/studentUsers').doc(this.authService.userDetails().email).ref.get().then(function (doc) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                                    var loading, _yield$loading$onDidD2, role, data, _loading2, _yield$_loading2$onDi, _role2, _data2, alert;

                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                      while (1) {
                                        switch (_context3.prev = _context3.next) {
                                          case 0:
                                            if (!(doc.data().status.toString() == "Active")) {
                                              _context3.next = 15;
                                              break;
                                            }

                                            _context3.next = 3;
                                            return this.loadingController.create({
                                              message: 'Logging in...',
                                              duration: 2000
                                            });

                                          case 3:
                                            loading = _context3.sent;
                                            _context3.next = 6;
                                            return loading.present();

                                          case 6:
                                            _context3.next = 8;
                                            return loading.onDidDismiss();

                                          case 8:
                                            _yield$loading$onDidD2 = _context3.sent;
                                            role = _yield$loading$onDidD2.role;
                                            data = _yield$loading$onDidD2.data;
                                            console.log('Loading dismissed!');
                                            this.navCtrl.navigateForward("tabs/home");
                                            _context3.next = 32;
                                            break;

                                          case 15:
                                            this.authService.logoutUser();
                                            _context3.next = 18;
                                            return this.loadingController.create({
                                              message: 'Session Closing...',
                                              duration: 2000
                                            });

                                          case 18:
                                            _loading2 = _context3.sent;
                                            _context3.next = 21;
                                            return _loading2.present();

                                          case 21:
                                            _context3.next = 23;
                                            return _loading2.onDidDismiss();

                                          case 23:
                                            _yield$_loading2$onDi = _context3.sent;
                                            _role2 = _yield$_loading2$onDi.role;
                                            _data2 = _yield$_loading2$onDi.data;
                                            console.log('Loading dismissed!');
                                            _context3.next = 29;
                                            return this.alertController.create({
                                              header: 'Account Disabled',
                                              subHeader: 'Contact Program Office',
                                              message: 'You cannot access the NSBM HUB as your account is disabled !',
                                              buttons: ['OK']
                                            });

                                          case 29:
                                            alert = _context3.sent;
                                            _context3.next = 32;
                                            return alert.present();

                                          case 32:
                                          case "end":
                                            return _context3.stop();
                                        }
                                      }
                                    }, _callee3, this);
                                  }));
                                });

                              case 3:
                              case "end":
                                return _context4.stop();
                            }
                          }
                        }, _callee4, this);
                      }));
                    }, function (err) {
                      _this3.errorMessage = err.message;
                    });

                  case 1:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "goToRegisterPage",
        value: function goToRegisterPage() {
          this.navCtrl.navigateForward("/signup");
        }
      }, {
        key: "goToGuestPage",
        value: function goToGuestPage() {
          console.log("Guest Logging in...");
          this.navCtrl.navigateForward("/guest");
        }
      }, {
        key: "goToLoginPage",
        value: function goToLoginPage() {
          console.log("Guest Logging in...");
          this.navCtrl.navigateForward("/welcome");
        }
      }]);

      return Login;
    }();

    Login.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: _services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
      }, {
        type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }];
    };

    Login = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
      selector: "app-login",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login.page.scss */
      "./src/app/login/login.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])], Login);
    /***/
  }
}]);
//# sourceMappingURL=login-login-module-es5.js.map