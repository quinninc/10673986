(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notes-notes-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/notes/notes.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/notes/notes.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"warning\">\r\n    <ion-title>\r\n      Notes\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content color=\"dark\">\r\n\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"getNotes($event)\">\r\n    <ion-refresher-content></ion-refresher-content>\r\n  </ion-refresher>\r\n\r\n\r\n  <div class=\"bg-img\">\r\n    <h1><b>Take you notes here!</b></h1>\r\n    <h5>{{ currentDate }}</h5>\r\n\r\n    <ion-card *ngFor=\"let Note of Notes\">\r\n      <ion-item-sliding>\r\n        <ion-item lines=\"none\">\r\n          <ion-checkbox (ionChange)=\"changeCheckState(Note)\" color=\"success\" [(ngModel)]=\"Note.checked\" slot=\"start\">\r\n          </ion-checkbox>\r\n          <ion-label>\r\n            <h2 *ngIf=\"!Note.checked\">{{ Note.text }}</h2>\r\n            <h2 *ngIf=\"Note.checked\" style=\"text-decoration:line-through;\">{{ Note.text }}</h2>\r\n            <p>{{ Note.hour }}</p>\r\n          </ion-label>\r\n        </ion-item>\r\n        <ion-item-options side=\"end\">\r\n          <ion-item-option color=\"danger\" (click)=\"deleteNote(Note)\">X\r\n          </ion-item-option>\r\n        </ion-item-options>\r\n      </ion-item-sliding>\r\n    </ion-card>\r\n\r\n    <ion-card *ngIf=\"addNote\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input [(ngModel)]=\"Note\" placeholder=\"Take your note\"></ion-input>\r\n        <ion-button (click)=\"addNoteToFirebase()\" shape=\"round\" color=\"primary\" slot=\"end\">+\r\n        </ion-button>\r\n      </ion-item>\r\n    </ion-card>\r\n\r\n    <ion-button *ngIf=\"!addNote\" (click)=\"showForm()\" expand=\"block\" class=\"add-button\" color=\"light\">\r\n      <ion-icon name=\"add\" slot=\"start\"></ion-icon>\r\n      Add a Note\r\n    </ion-button>\r\n\r\n    <ion-button *ngIf=\"addNote\" (click)=\"showForm()\" expand=\"block\" class=\"add-button\" color=\"danger\">\r\n      <ion-icon name=\"close\" slot=\"start\"></ion-icon>\r\n      Discard Note\r\n    </ion-button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/notes/notes.module.ts":
/*!***************************************!*\
  !*** ./src/app/notes/notes.module.ts ***!
  \***************************************/
/*! exports provided: notesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notesPageModule", function() { return notesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _notes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notes.page */ "./src/app/notes/notes.page.ts");







let notesPageModule = class notesPageModule {
};
notesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _notes_page__WEBPACK_IMPORTED_MODULE_6__["notesPage"] }])
        ],
        declarations: [_notes_page__WEBPACK_IMPORTED_MODULE_6__["notesPage"]]
    })
], notesPageModule);



/***/ }),

/***/ "./src/app/notes/notes.page.scss":
/*!***************************************!*\
  !*** ./src/app/notes/notes.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".bg-img {\n  padding: 1px;\n  height: 100%;\n  width: 100%;\n  background-image: url('notes.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\nion-card {\n  margin-bottom: 0px;\n  margin-top: 6px;\n}\n\nh1 {\n  margin-left: 20px;\n  font-size: 30px;\n}\n\nh5 {\n  margin-left: 20px;\n  margin-top: -7px;\n  font-size: medium;\n  margin-bottom: 15px;\n  text-transform: capitalize;\n}\n\n.add-button {\n  position: absolute;\n  bottom: 5px;\n  width: 96%;\n  margin-left: 2%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90ZXMvQzpcXFVzZXJzXFxyYW51bFxcRG9jdW1lbnRzXFxHaXRIdWJcXDJuZCBZZWFyXFwybmQgU2VtZXN0ZXJcXFVPUF9TRV9ZMlMxLVBVU0wyMDAzX0lOVEVHUkFUSU5HX1BST0pFQ1RcXEFwcGxpY2F0aW9uXFxNb2JpbGUgQXBwcyAoUmFudWwgLSAxMDY3Mzk4NilcXFdvcmtzcGFjZVxcTlNCTS1IVUIvc3JjXFxhcHBcXG5vdGVzXFxub3Rlcy5wYWdlLnNjc3MiLCJzcmMvYXBwL25vdGVzL25vdGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ0NKOztBRENBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0FDRUo7O0FEQUE7RUFDSSxpQkFBQTtFQUNBLGVBQUE7QUNHSjs7QUREQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsMEJBQUE7QUNJSjs7QURGQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9ub3Rlcy9ub3Rlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmctaW1nIHtcclxuICAgIHBhZGRpbmc6IDFweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvbm90ZXMuanBnJyk7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5pb24tY2FyZCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA2cHg7XHJcbn1cclxuaDEge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuaDUge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtN3B4O1xyXG4gICAgZm9udC1zaXplOiBtZWRpdW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuLmFkZC1idXR0b24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiA1cHg7XHJcbiAgICB3aWR0aDogOTYlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIlO1xyXG59IiwiLmJnLWltZyB7XG4gIHBhZGRpbmc6IDFweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL25vdGVzLmpwZ1wiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuaW9uLWNhcmQge1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi10b3A6IDZweDtcbn1cblxuaDEge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG5oNSB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICBtYXJnaW4tdG9wOiAtN3B4O1xuICBmb250LXNpemU6IG1lZGl1bTtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5hZGQtYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDVweDtcbiAgd2lkdGg6IDk2JTtcbiAgbWFyZ2luLWxlZnQ6IDIlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/notes/notes.page.ts":
/*!*************************************!*\
  !*** ./src/app/notes/notes.page.ts ***!
  \*************************************/
/*! exports provided: notesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notesPage", function() { return notesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../services.service */ "./src/app/services.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");





let notesPage = class notesPage {
    constructor(database, firebase, firestore) {
        this.database = database;
        this.firebase = firebase;
        this.firestore = firestore;
        this.Note = '';
        this.Notes = [];
        this.StudentID = '';
        const date = new Date();
        const options = { weekday: 'long', month: 'long', day: 'numeric' };
        this.currentDate = date.toLocaleDateString('en-GB', options);
        this.autorefresh(event);
    }
    showForm() {
        this.addNote = !this.addNote;
        this.Note = '';
    }
    addNoteToFirebase() {
        this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
            this.StudentID = doc.data().nsbmStudentID;
            this.database.list('/PrivateNotes/' + this.StudentID).push({
                text: this.Note,
                date: new Date().toISOString(),
                checked: false
            });
            this.showForm();
        });
    }
    changeCheckState(ev) {
        this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
            this.StudentID = doc.data().nsbmStudentID;
            console.log('checked: ' + ev.checked);
            this.database.object('/PrivateNotes/' + this.StudentID + ev.key + '/checked/').set(ev.checked);
        });
    }
    deleteNote(Note) {
        this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
            this.StudentID = doc.data().nsbmStudentID;
            this.database.list('/PrivateNotes/' + this.StudentID).remove(Note.key);
        });
    }
    getNotes(event) {
        this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
            this.StudentID = doc.data().nsbmStudentID;
            console.log('Begin async operation');
            setTimeout(() => {
                this.database.list('/PrivateNotes/' + this.StudentID).snapshotChanges(['child_added', 'child_removed']).subscribe(actions => {
                    this.Notes = [];
                    actions.forEach(action => {
                        this.Notes.push({
                            key: action.key,
                            text: action.payload.exportVal().text,
                            hour: action.payload.exportVal().date.substring(11, 16),
                            checked: action.payload.exportVal().checked
                        });
                    });
                });
                console.log('Async operation has ended');
                event.target.complete();
            }, 3000);
        });
    }
    autorefresh(event) {
        this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
            this.StudentID = doc.data().nsbmStudentID;
            this.database.list('/PrivateNotes/' + this.StudentID).snapshotChanges(['child_added', 'child_removed']).subscribe(actions => {
                this.Notes = [];
                actions.forEach(action => {
                    this.Notes.push({
                        key: action.key,
                        text: action.payload.exportVal().text,
                        hour: action.payload.exportVal().date.substring(11, 16),
                        checked: action.payload.exportVal().checked
                    });
                });
            });
        });
    }
};
notesPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"] },
    { type: _services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"] }
];
notesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-notes',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./notes.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/notes/notes.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./notes.page.scss */ "./src/app/notes/notes.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"], _services_service__WEBPACK_IMPORTED_MODULE_1__["ServicesService"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]])
], notesPage);



/***/ })

}]);
//# sourceMappingURL=notes-notes-module-es2015.js.map