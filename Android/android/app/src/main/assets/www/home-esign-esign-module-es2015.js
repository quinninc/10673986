(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-esign-esign-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/esign/esign.page.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/esign/esign.page.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <div class=\"ion-page\" id=\"main-content\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-buttons slot=\"start\">\n          <ion-menu-button></ion-menu-button>\n        </ion-buttons>\n        <ion-title>eSign</ion-title>\n      </ion-toolbar>\n    </ion-header>\n    <ion-content class=\"ion-padding\">\n      <div name=Sessions>\n        <ion-card *ngFor=\"let item of session\">\n          <ion-card-title>\n            <ion-card-header>Sign Your Attendance Online</ion-card-header>\n          </ion-card-title>\n          <ion-card-content>\n            <ion-input [(ngModel)]=\"SessionCode\" placeholder=\"Enter your Session Code\">Session Code: </ion-input>\n            <br>\n            <ion-label>Module: {{ item.Module }}</ion-label><br>\n            <ion-label>Session: {{ item.Session }}</ion-label><br>\n            <ion-label>Date: {{ item.Date }}</ion-label><br>\n            <ion-label>Hall: {{ item.Hall }}</ion-label><br>\n            <br>\n            <ion-label *ngIf=\"location\">Student Location: {{ item.Hall }}<ion-icon name=\"checkmark-done-circle-outline\"\n                color=\"success\"></ion-icon>\n            </ion-label>\n            <ion-label *ngIf=\"!location\">Student Location: outside<ion-icon name=\"close-circle-outline\" color=\"danger\">\n              </ion-icon>\n            </ion-label>\n            <p *ngIf=\"location\">we have detected you are inside the hall</p>\n            <p *ngIf=\"!location\">we cannot detect your inside the hall enable location services and try again.</p>\n            <ion-button [disabled]=\"item.SessionCode != SessionCode\" (click)=\"addAttendaceToFirebase()\" shape=\"round\"\n              color=\"light\" slot=\"end\">Mark Attendance<ion-icon slot=\"icon-only\" name=\"checkmark-done-circle\">\n              </ion-icon>\n            </ion-button>\n          </ion-card-content>\n        </ion-card>\n      </div>\n      <div name=\"NoSessions\" *ngIf=\"nosession\" align='center'>\n        <ion-card>\n          <ion-card-content>\n            <h1>No sessions today, check your calendar.</h1>\n            <ion-img src=\"/assets/internals/nosession.svg\" style=\"height: 200px;\"></ion-img>\n          </ion-card-content>\n        </ion-card>\n      </div>\n      <div name=\"CompletedSessions\" *ngIf=\"signed\" align='center'>\n        <ion-card>\n          <ion-card-content>\n            <h1>You have already signed for all your sessions today.</h1>\n            <ion-img src=\"/assets/internals/done.svg\" style=\"height: 200px;\"></ion-img>\n          </ion-card-content>\n        </ion-card>\n      </div>\n\n    </ion-content>\n  </div>\n</ion-app>");

/***/ }),

/***/ "./src/app/home/esign/esign-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/home/esign/esign-routing.module.ts ***!
  \****************************************************/
/*! exports provided: EsignPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EsignPageRoutingModule", function() { return EsignPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _esign_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./esign.page */ "./src/app/home/esign/esign.page.ts");




const routes = [
    {
        path: '',
        component: _esign_page__WEBPACK_IMPORTED_MODULE_3__["EsignPage"]
    }
];
let EsignPageRoutingModule = class EsignPageRoutingModule {
};
EsignPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EsignPageRoutingModule);



/***/ }),

/***/ "./src/app/home/esign/esign.module.ts":
/*!********************************************!*\
  !*** ./src/app/home/esign/esign.module.ts ***!
  \********************************************/
/*! exports provided: EsignPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EsignPageModule", function() { return EsignPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _esign_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./esign-routing.module */ "./src/app/home/esign/esign-routing.module.ts");
/* harmony import */ var _esign_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./esign.page */ "./src/app/home/esign/esign.page.ts");







let EsignPageModule = class EsignPageModule {
};
EsignPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _esign_routing_module__WEBPACK_IMPORTED_MODULE_5__["EsignPageRoutingModule"]
        ],
        declarations: [_esign_page__WEBPACK_IMPORTED_MODULE_6__["EsignPage"]]
    })
], EsignPageModule);



/***/ }),

/***/ "./src/app/home/esign/esign.page.scss":
/*!********************************************!*\
  !*** ./src/app/home/esign/esign.page.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvZXNpZ24vZXNpZ24ucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/home/esign/esign.page.ts":
/*!******************************************!*\
  !*** ./src/app/home/esign/esign.page.ts ***!
  \******************************************/
/*! exports provided: EsignPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EsignPage", function() { return EsignPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services.service */ "./src/app/services.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");






let EsignPage = class EsignPage {
    constructor(firestore, router, firebase, navCtrl, toastController) {
        this.firestore = firestore;
        this.router = router;
        this.firebase = firebase;
        this.navCtrl = navCtrl;
        this.toastController = toastController;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).set({
                accountActivity: 'Online',
            }, { merge: true });
            this.firestore.collection('userActivityMonitoring').add({
                loginDateTime: new Date(),
                userId: this.firebase.userDetails().email,
            });
            this.fetchdata();
        });
    }
    fetchdata() {
        var Faculty;
        var ModuleCode;
        var LocationCheck;
        this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
            if (doc.exists) {
                console.log(doc.data().faculty);
                Faculty = doc.data().faculty;
                // console.log(DegreeCode + Faculty + Batch)
                this.firebase.fetchSession(Faculty).subscribe(data => {
                    console.log(doc.data());
                    if (!doc.exists) {
                        // console.log('NO SESSION FOR TODAY ')
                        this.nosession = true;
                    }
                    else {
                        this.nosession = false;
                        this.check = data.map(e => {
                            ModuleCode = e.payload.doc.data()['moduleCode'] + "-" + e.payload.doc.data()['moduleTitle'];
                            // console.log(ModuleCode)
                            this.firestore.collection('Attendance/History/' + ModuleCode).doc(this.firebase.userDetails().email).ref.get().then((doc) => {
                                if (doc.exists) {
                                    // console.log(doc.data())
                                    // console.log('ALREADY SIGNED')
                                    this.signed = true;
                                }
                                else {
                                    this.signed = false;
                                    this.firebase.fetchSession(Faculty).subscribe(data => {
                                        // console.log(Batch + '' + Faculty + '' + LectureDate)
                                        this.session = data.map(e => {
                                            ModuleCode = e.payload.doc.data()['moduleCode'] + "-" + e.payload.doc.data()['moduleTitle'];
                                            LocationCheck = e.payload.doc.data()['LocationCheck'];
                                            return {
                                                id: e.payload.doc.id,
                                                SessionCode: e.payload.doc.data()['SessionCode'],
                                                Module: e.payload.doc.data()['moduleCode'] + " " + e.payload.doc.data()['moduleTitle'],
                                                Session: e.payload.doc.data()['Session'],
                                                Date: e.payload.doc.data()['startDateTime'].toDate(),
                                                Hall: e.payload.doc.data()['lectureHall'],
                                                Lecturer: e.payload.doc.data()['lecturer'],
                                            };
                                        });
                                        // console.log(this.session);
                                        // console.log(ModuleCode)
                                        this.firestore.collection('/faculties/' + Faculty + '/lectureSessions/').doc(ModuleCode).ref.get().then((doc) => {
                                            if (doc.exists) {
                                                // console.log(doc.data());
                                                this.CloudCode = doc.data().SessionCode;
                                                // console.log(this.CloudCode)
                                            }
                                            else {
                                                // console.log("There is no document!");
                                            }
                                        }).catch(function (error) {
                                            // console.log("There was an error getting your document:", error);
                                        });
                                    });
                                }
                            });
                        });
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var NSBMPOS = 'lat: 6.8211, lng: 80.0409';
                            if (LocationCheck = true) {
                                if (pos.toString() == NSBMPOS) {
                                    this.location = true;
                                }
                                else {
                                    // this.location = false;
                                }
                            }
                            if (LocationCheck = false) {
                                this.location = true;
                            }
                        });
                    }
                });
            }
            else {
                // console.log("There is no document!");
            }
        }).catch(function (error) {
            // console.log("There was an error getting your document:", error);
        });
    }
    addAttendaceToFirebase() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var Faculty;
            var ModuleCode;
            if (this.CloudCode == this.SessionCode) {
                this.firestore.collection('/users/userTypes/studentUsers').doc(this.firebase.userDetails().email).ref.get().then((doc) => {
                    if (doc.exists) {
                        // console.log(doc.data());
                        Faculty = doc.data().faculty;
                        this.firebase.fetchSession(Faculty).subscribe(data => {
                            // console.log(Batch + '' + Faculty + '' + LectureDate)
                            this.session = data.map(e => {
                                ModuleCode = e.payload.doc.data()['moduleCode'] + "-" + e.payload.doc.data()['moduleTitle'];
                                return {
                                    id: e.payload.doc.id,
                                    SessionCode: e.payload.doc.data()['SessionCode'],
                                    Module: e.payload.doc.data()['moduleCode'] + " " + e.payload.doc.data()['moduleTitle'],
                                    Session: e.payload.doc.data()['Session'],
                                    Date: e.payload.doc.data()['startDateTime'].toDate(),
                                    Hall: e.payload.doc.data()['lectureHall'],
                                    Lecturer: e.payload.doc.data()['lecturer'],
                                };
                            });
                            // SENDING to FIRESTORE
                            console.log(ModuleCode);
                            this.firebase.sendAttendance(ModuleCode, this.firebase.userDetails().email).then((resp) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                const toast = yield this.toastController.create({
                                    message: 'Your Attendance has been accepted',
                                    duration: 2000
                                });
                                toast.present();
                                this.fetchdata();
                            }))
                                .catch((error) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                // console.log(error);
                                const toast = yield this.toastController.create({
                                    message: 'Error in Network, check back later.',
                                    duration: 2000
                                });
                                toast.present();
                            }));
                        });
                    }
                });
            }
            else {
                const toast = yield this.toastController.create({
                    message: 'Session Code is Wrong ! | TRY AGAIN',
                    duration: 2000
                });
                toast.present();
                this.Alert = "Session Code is Wrong ! | TRY AGAIN";
            }
        });
    }
};
EsignPage.ctorParameters = () => [
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] }
];
EsignPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-esign',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./esign.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/esign/esign.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./esign.page.scss */ "./src/app/home/esign/esign.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], src_app_services_service__WEBPACK_IMPORTED_MODULE_4__["ServicesService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
], EsignPage);



/***/ })

}]);
//# sourceMappingURL=home-esign-esign-module-es2015.js.map