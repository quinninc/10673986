(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["welcome-welcome-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome/welcome.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/welcome/welcome.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\r\n  <ion-app>\r\n    <ion-slides pager=\"false\" [options]=\"slideOpts\" class=\"slides\" style=\"margin: 5px;\">\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_1.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_2.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_3.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_4.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_5.png\">\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n    <ion-footer>\r\n        <ion-button (click)=\"goToLoginPage()\" color=\"primary\" shape=\"round\" expand=\"block\">\r\n          Login\r\n        </ion-button>\r\n        <ion-button (click)=\"goToRegisterPage()\" shape=\"round\" color=\"primary\" expand=\"block\">\r\n          Register\r\n        </ion-button>\r\n        <ion-button (click)=\"goToGuestPage()\" shape=\"round\" color=\"primary\" expand=\"block\">\r\n          Guests\r\n        </ion-button>\r\n    </ion-footer>\r\n\r\n  </ion-app>\r\n  </body>");

/***/ }),

/***/ "./src/app/welcome/welcome.module.ts":
/*!*******************************************!*\
  !*** ./src/app/welcome/welcome.module.ts ***!
  \*******************************************/
/*! exports provided: WelcomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomePageModule", function() { return WelcomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _welcome_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./welcome.page */ "./src/app/welcome/welcome.page.ts");







const routes = [
    {
        path: '',
        component: _welcome_page__WEBPACK_IMPORTED_MODULE_6__["WelcomePage"]
    }
];
let WelcomePageModule = class WelcomePageModule {
};
WelcomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_welcome_page__WEBPACK_IMPORTED_MODULE_6__["WelcomePage"]]
    })
], WelcomePageModule);



/***/ }),

/***/ "./src/app/welcome/welcome.page.scss":
/*!*******************************************!*\
  !*** ./src/app/welcome/welcome.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUvd2VsY29tZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/welcome/welcome.page.ts":
/*!*****************************************!*\
  !*** ./src/app/welcome/welcome.page.ts ***!
  \*****************************************/
/*! exports provided: WelcomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomePage", function() { return WelcomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../services.service */ "./src/app/services.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/es2015/index.js");






;

let WelcomePage = class WelcomePage {
    constructor(navCtrl, authService, formBuilder, loadingController, alertController, firestore) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.firestore = firestore;
        this.errorMessage = "";
        this.show = false;
        this.slideOpts = {
            initialSlide: 0,
            slidesPerView: 1,
            autoplay: true,
            speed: 400
        };
    }
    ngOnInit() {
        firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().onAuthStateChanged((user) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (user) {
                this.firestore.collection('/users/userTypes/studentUsers').doc(this.authService.userDetails().email).ref.get().then((doc) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    if (doc.data().status.toString() == "Active") {
                        // User is signed in.
                        console.log('User is signed in');
                        const loading = yield this.loadingController.create({
                            message: 'Please wait...',
                            duration: 2000
                        });
                        yield loading.present();
                        const { role, data } = yield loading.onDidDismiss();
                        console.log('Loading dismissed!');
                        this.userEmail = this.authService.userDetails().email;
                        this.navCtrl.navigateForward("tabs/home");
                    }
                    else {
                        this.authService.logoutUser();
                        const loading = yield this.loadingController.create({
                            message: 'Session Closing...',
                            duration: 2000
                        });
                        yield loading.present();
                        const { role, data } = yield loading.onDidDismiss();
                        console.log('Loading dismissed!');
                        const alert = yield this.alertController.create({
                            header: 'Account Disabled',
                            subHeader: 'Contact Program Office',
                            message: 'You cannot access the NSBM HUB as your account is disabled !',
                            buttons: ['OK']
                        });
                        yield alert.present();
                    }
                }));
            }
            else {
                // No user is signed in.
                console.log('User is NOT signed in');
            }
        }));
    }
    ;
    loginUser(value) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Logging in...',
                duration: 2000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
            this.authService.loginUser(value).then((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                console.log(res);
                this.errorMessage = "";
                this.navCtrl.navigateForward("tabs/home");
            }), err => {
                this.errorMessage = err.message;
            });
        });
    }
    goToRegisterPage() {
        this.navCtrl.navigateForward("/signup");
    }
    goToGuestPage() {
        console.log("Guest Logging in...");
        this.navCtrl.navigateForward("/guest");
    }
    goToLoginPage() {
        console.log("Guest Logging in...");
        this.navCtrl.navigateForward("/login");
    }
};
WelcomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _services_service__WEBPACK_IMPORTED_MODULE_3__["ServicesService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"] }
];
WelcomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-welcome',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./welcome.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome/welcome.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./welcome.page.scss */ "./src/app/welcome/welcome.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"],
        _services_service__WEBPACK_IMPORTED_MODULE_3__["ServicesService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
        _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]])
], WelcomePage);

// let disconnectSubscription = this.network.onDisconnect().subscribe(async () => {
//   console.log('network was disconnected :-(');
//   const alert = await this.alertController.create({
//     header: 'No Network',
//     subHeader: 'Connectivity Lost',
//     message: 'We are not sensing any network connections, please connect to continue using.',
//     backdropDismiss: false
//   });
//   await alert.present();
// });
// disconnectSubscription.unsubscribe();
// let connectSubscription = this.network.onConnect().subscribe(() => {
//   console.log('network connected!');
//   setTimeout(async () => {
//     console.log('we got a wifi connection, woohoo!');
//     const alert = await this.alertController.create({
//       header: 'Connected',
//       subHeader: 'Connection Established',
//       message: 'Please wait while we preparing your content.',
//       backdropDismiss: false
//     });
//     await alert.present();
//   }, 2000);
// });
// // stop connect watch
// connectSubscription.unsubscribe();


/***/ })

}]);
//# sourceMappingURL=welcome-welcome-module-es2015.js.map