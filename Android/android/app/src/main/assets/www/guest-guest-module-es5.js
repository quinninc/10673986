function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["guest-guest-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/guest/guest.page.html":
  /*!*****************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/guest/guest.page.html ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGuestGuestPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button><ion-icon>←</ion-icon>\r\n      </ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Guest</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n<ion-card>\r\n  <ion-card-title>Library</ion-card-title>\r\n    <ion-img src=\"assets/library.jpg\" align=\"middle\"></ion-img>\r\n</ion-card>\r\n<ion-card>\r\n  <ion-card-title>Recreation Centre</ion-card-title>\r\n    <ion-img src=\"assets/recreation.jpg\" align=\"middle\"></ion-img>\r\n</ion-card>\r\n<ion-card>\r\n  <ion-card-title>WiFi</ion-card-title>\r\n    <ion-img src=\"assets/wifi.jpg\"align=\" middle\"></ion-img>\r\n</ion-card>\r\n<ion-card>\r\n  <ion-card-title>Clubs and Events</ion-card-title>\r\n    <ion-img src=\"/assets/clubs.jpg\" align=\"middle\"></ion-img>\r\n</ion-card>\r\n</ion-content>\r\n";
    /***/
  },

  /***/
  "./src/app/guest/guest.module.ts":
  /*!***************************************!*\
    !*** ./src/app/guest/guest.module.ts ***!
    \***************************************/

  /*! exports provided: GuestPageModule */

  /***/
  function srcAppGuestGuestModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GuestPageModule", function () {
      return GuestPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _guest_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./guest.page */
    "./src/app/guest/guest.page.ts");

    var routes = [{
      path: '',
      component: _guest_page__WEBPACK_IMPORTED_MODULE_6__["GuestPage"]
    }];

    var GuestPageModule = function GuestPageModule() {
      _classCallCheck(this, GuestPageModule);
    };

    GuestPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_guest_page__WEBPACK_IMPORTED_MODULE_6__["GuestPage"]]
    })], GuestPageModule);
    /***/
  },

  /***/
  "./src/app/guest/guest.page.scss":
  /*!***************************************!*\
    !*** ./src/app/guest/guest.page.scss ***!
    \***************************************/

  /*! exports provided: default */

  /***/
  function srcAppGuestGuestPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2d1ZXN0L2d1ZXN0LnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/guest/guest.page.ts":
  /*!*************************************!*\
    !*** ./src/app/guest/guest.page.ts ***!
    \*************************************/

  /*! exports provided: GuestPage */

  /***/
  function srcAppGuestGuestPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GuestPage", function () {
      return GuestPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var GuestPage = /*#__PURE__*/function () {
      function GuestPage() {
        _classCallCheck(this, GuestPage);
      }

      _createClass(GuestPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return GuestPage;
    }();

    GuestPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-guest',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./guest.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/guest/guest.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./guest.page.scss */
      "./src/app/guest/guest.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], GuestPage);
    /***/
  }
}]);
//# sourceMappingURL=guest-guest-module-es5.js.map