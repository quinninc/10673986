function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["welcome-welcome-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome/welcome.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/welcome/welcome.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppWelcomeWelcomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<body>\r\n  <ion-app>\r\n    <ion-slides pager=\"false\" [options]=\"slideOpts\" class=\"slides\" style=\"margin: 5px;\">\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_1.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_2.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_3.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_4.png\">\r\n      </ion-slide>\r\n      <ion-slide>\r\n        <img src=\"/assets/slides/Slide_5.png\">\r\n      </ion-slide>\r\n    </ion-slides>\r\n\r\n    <ion-footer>\r\n        <ion-button (click)=\"goToLoginPage()\" color=\"primary\" shape=\"round\" expand=\"block\">\r\n          Login\r\n        </ion-button>\r\n        <ion-button (click)=\"goToRegisterPage()\" shape=\"round\" color=\"primary\" expand=\"block\">\r\n          Register\r\n        </ion-button>\r\n        <ion-button (click)=\"goToGuestPage()\" shape=\"round\" color=\"primary\" expand=\"block\">\r\n          Guests\r\n        </ion-button>\r\n    </ion-footer>\r\n\r\n  </ion-app>\r\n  </body>";
    /***/
  },

  /***/
  "./src/app/welcome/welcome.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/welcome/welcome.module.ts ***!
    \*******************************************/

  /*! exports provided: WelcomePageModule */

  /***/
  function srcAppWelcomeWelcomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WelcomePageModule", function () {
      return WelcomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _welcome_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./welcome.page */
    "./src/app/welcome/welcome.page.ts");

    var routes = [{
      path: '',
      component: _welcome_page__WEBPACK_IMPORTED_MODULE_6__["WelcomePage"]
    }];

    var WelcomePageModule = function WelcomePageModule() {
      _classCallCheck(this, WelcomePageModule);
    };

    WelcomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_welcome_page__WEBPACK_IMPORTED_MODULE_6__["WelcomePage"]]
    })], WelcomePageModule);
    /***/
  },

  /***/
  "./src/app/welcome/welcome.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/welcome/welcome.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppWelcomeWelcomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUvd2VsY29tZS5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/welcome/welcome.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/welcome/welcome.page.ts ***!
    \*****************************************/

  /*! exports provided: WelcomePage */

  /***/
  function srcAppWelcomeWelcomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WelcomePage", function () {
      return WelcomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _services_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./../services.service */
    "./src/app/services.service.ts");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.cjs.js");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/fire/firestore */
    "./node_modules/@angular/fire/firestore/es2015/index.js");

    ;

    var WelcomePage = /*#__PURE__*/function () {
      function WelcomePage(navCtrl, authService, formBuilder, loadingController, alertController, firestore) {
        _classCallCheck(this, WelcomePage);

        this.navCtrl = navCtrl;
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.firestore = firestore;
        this.errorMessage = "";
        this.show = false;
        this.slideOpts = {
          initialSlide: 0,
          slidesPerView: 1,
          autoplay: true,
          speed: 400
        };
      }

      _createClass(WelcomePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          firebase__WEBPACK_IMPORTED_MODULE_4__["auth"]().onAuthStateChanged(function (user) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this2 = this;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (user) {
                        this.firestore.collection('/users/userTypes/studentUsers').doc(this.authService.userDetails().email).ref.get().then(function (doc) {
                          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                            var loading, _yield$loading$onDidD, role, data, _loading, _yield$_loading$onDid, _role, _data, alert;

                            return regeneratorRuntime.wrap(function _callee$(_context) {
                              while (1) {
                                switch (_context.prev = _context.next) {
                                  case 0:
                                    if (!(doc.data().status.toString() == "Active")) {
                                      _context.next = 17;
                                      break;
                                    }

                                    // User is signed in.
                                    console.log('User is signed in');
                                    _context.next = 4;
                                    return this.loadingController.create({
                                      message: 'Please wait...',
                                      duration: 2000
                                    });

                                  case 4:
                                    loading = _context.sent;
                                    _context.next = 7;
                                    return loading.present();

                                  case 7:
                                    _context.next = 9;
                                    return loading.onDidDismiss();

                                  case 9:
                                    _yield$loading$onDidD = _context.sent;
                                    role = _yield$loading$onDidD.role;
                                    data = _yield$loading$onDidD.data;
                                    console.log('Loading dismissed!');
                                    this.userEmail = this.authService.userDetails().email;
                                    this.navCtrl.navigateForward("tabs/home");
                                    _context.next = 34;
                                    break;

                                  case 17:
                                    this.authService.logoutUser();
                                    _context.next = 20;
                                    return this.loadingController.create({
                                      message: 'Session Closing...',
                                      duration: 2000
                                    });

                                  case 20:
                                    _loading = _context.sent;
                                    _context.next = 23;
                                    return _loading.present();

                                  case 23:
                                    _context.next = 25;
                                    return _loading.onDidDismiss();

                                  case 25:
                                    _yield$_loading$onDid = _context.sent;
                                    _role = _yield$_loading$onDid.role;
                                    _data = _yield$_loading$onDid.data;
                                    console.log('Loading dismissed!');
                                    _context.next = 31;
                                    return this.alertController.create({
                                      header: 'Account Disabled',
                                      subHeader: 'Contact Program Office',
                                      message: 'You cannot access the NSBM HUB as your account is disabled !',
                                      buttons: ['OK']
                                    });

                                  case 31:
                                    alert = _context.sent;
                                    _context.next = 34;
                                    return alert.present();

                                  case 34:
                                  case "end":
                                    return _context.stop();
                                }
                              }
                            }, _callee, this);
                          }));
                        });
                      } else {
                        // No user is signed in.
                        console.log('User is NOT signed in');
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
        }
      }, {
        key: "loginUser",
        value: function loginUser(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this3 = this;

            var loading, _yield$loading$onDidD2, role, data;

            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.loadingController.create({
                      message: 'Logging in...',
                      duration: 2000
                    });

                  case 2:
                    loading = _context4.sent;
                    _context4.next = 5;
                    return loading.present();

                  case 5:
                    _context4.next = 7;
                    return loading.onDidDismiss();

                  case 7:
                    _yield$loading$onDidD2 = _context4.sent;
                    role = _yield$loading$onDidD2.role;
                    data = _yield$loading$onDidD2.data;
                    console.log('Loading dismissed!');
                    this.authService.loginUser(value).then(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                          while (1) {
                            switch (_context3.prev = _context3.next) {
                              case 0:
                                console.log(res);
                                this.errorMessage = "";
                                this.navCtrl.navigateForward("tabs/home");

                              case 3:
                              case "end":
                                return _context3.stop();
                            }
                          }
                        }, _callee3, this);
                      }));
                    }, function (err) {
                      _this3.errorMessage = err.message;
                    });

                  case 12:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "goToRegisterPage",
        value: function goToRegisterPage() {
          this.navCtrl.navigateForward("/signup");
        }
      }, {
        key: "goToGuestPage",
        value: function goToGuestPage() {
          console.log("Guest Logging in...");
          this.navCtrl.navigateForward("/guest");
        }
      }, {
        key: "goToLoginPage",
        value: function goToLoginPage() {
          console.log("Guest Logging in...");
          this.navCtrl.navigateForward("/login");
        }
      }]);

      return WelcomePage;
    }();

    WelcomePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _services_service__WEBPACK_IMPORTED_MODULE_3__["ServicesService"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
      }, {
        type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]
      }];
    };

    WelcomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-welcome',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./welcome.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/welcome/welcome.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./welcome.page.scss */
      "./src/app/welcome/welcome.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _services_service__WEBPACK_IMPORTED_MODULE_3__["ServicesService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]])], WelcomePage); // let disconnectSubscription = this.network.onDisconnect().subscribe(async () => {
    //   console.log('network was disconnected :-(');
    //   const alert = await this.alertController.create({
    //     header: 'No Network',
    //     subHeader: 'Connectivity Lost',
    //     message: 'We are not sensing any network connections, please connect to continue using.',
    //     backdropDismiss: false
    //   });
    //   await alert.present();
    // });
    // disconnectSubscription.unsubscribe();
    // let connectSubscription = this.network.onConnect().subscribe(() => {
    //   console.log('network connected!');
    //   setTimeout(async () => {
    //     console.log('we got a wifi connection, woohoo!');
    //     const alert = await this.alertController.create({
    //       header: 'Connected',
    //       subHeader: 'Connection Established',
    //       message: 'Please wait while we preparing your content.',
    //       backdropDismiss: false
    //     });
    //     await alert.present();
    //   }, 2000);
    // });
    // // stop connect watch
    // connectSubscription.unsubscribe();

    /***/
  }
}]);
//# sourceMappingURL=welcome-welcome-module-es5.js.map